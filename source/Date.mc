using Toybox.WatchUi;
using Toybox.System;
using Toybox.Application;
using Toybox.Time;
using Toybox.Time.Gregorian;

class Date extends WatchUi.Drawable {
	
	function initialize(params) {
		Drawable.initialize(params);
	}
	
	function draw(dc) {
		
		var now = Gregorian.info(Time.now(), Time.FORMAT_SHORT);
		var dayOfWeek = now.day_of_week;
		var day = now.day.format("%d"); 
		var month = now.month;
		
		var rezStrings = Rez.Strings;
			
		var resourceArray = [
			rezStrings.Sun,
			rezStrings.Mon,
			rezStrings.Tue,
			rezStrings.Wed,
			rezStrings.Thu,
			rezStrings.Fri,
			rezStrings.Sat
		];
		
		var dayOfWeekString = WatchUi.loadResource(resourceArray[dayOfWeek - 1]).toUpper();

		
		resourceArray = [
			rezStrings.Jan,
			rezStrings.Feb,
			rezStrings.Mar,
			rezStrings.Apr,
			rezStrings.May,
			rezStrings.Jun,
			rezStrings.Jul,
			rezStrings.Aug,
			rezStrings.Sep,
			rezStrings.Oct,
			rezStrings.Nov,
			rezStrings.Dec
		];
		
		var monthString = WatchUi.loadResource(resourceArray[month - 1]).toUpper();

		var dateString = Lang.format("$1$ $2$ $3$", [dayOfWeekString, day, monthString]);

		// Draw the battery status        
		dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
		dc.drawText((dc.getWidth() / 2), 50, Graphics.FONT_XTINY, dateString, Graphics.TEXT_JUSTIFY_CENTER);
		
	}

}