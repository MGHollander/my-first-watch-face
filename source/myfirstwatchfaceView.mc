using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Application;

class myfirstwatchfaceView extends WatchUi.WatchFace {

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    	// @TODO Datum toevoegen aan scherm
    	// @TODO Improve storage http://developer.garmin.com/connect-iq/programmers-guide/resource-compiler/#accessingpropertiesandsettings:application.properties
    	//       Investigate why to use the newer version

        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);
    }
    
    // Update part of the view in low power mode.
    function onPartialUpdate(dc) {
    	Time.drawSeconds(dc, true);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }
   
}
