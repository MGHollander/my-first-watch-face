using Toybox.WatchUi;
using Toybox.System;
using Toybox.Application;

class Time extends WatchUi.Drawable {
	
	function initialize(params) {
		Drawable.initialize(params);
	}
	
	function draw(dc) {
	
		var clockTime = System.getClockTime();
        var hour = clockTime.hour;
        var minute = clockTime.min.format("%02d");
        var seconds = clockTime.sec.format("%02d");
        var is24Hour = !System.getDeviceSettings().is24Hour;
        var amPm = null;
        
        if (!System.getDeviceSettings().is24Hour) {
            if (hour > 12) {
                hour = hour - 12;
                amPm = "PM";
            } else {
                amPm = "AM";
            }
        }
        
        hour = hour.format("%02d");
        
        // @TODO Monospace font zoeken voor lettertype, want nu staan de minuten lelijk als het bijvoorbeeld de 11 minuut in een uur is.
        //  	 Huiduge lettertype zelf monospace weergeven zou ook een optie kunnen zijn. 
		var fontHour = WatchUi.loadResource(Rez.Fonts.FontSteelfishExtraBold);
		var fontMinute = WatchUi.loadResource(Rez.Fonts.FontSteelfishRegular);
		
		var timePositionX = dc.getWidth() / 2;
		var hourPositionY = (dc.getHeight() / 2) - (dc.getFontHeight(fontHour) / 2);
		var minutePositionY = (dc.getHeight() / 2) - (dc.getFontHeight(fontMinute) / 2); 
	
		// Draw the hour
		dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
		dc.drawText(timePositionX, hourPositionY, fontHour, hour, Graphics.TEXT_JUSTIFY_RIGHT);
		
		// Draw minutes
		dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
		dc.drawText(timePositionX, minutePositionY, fontMinute, minute, Graphics.TEXT_JUSTIFY_LEFT);
		
		// Draw AM / PM
		if (amPm != null) {
			dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
			// @TODO x coordinaat vervangen door dynamische waarde
			dc.drawText(175, minutePositionY, Graphics.FONT_SYSTEM_XTINY, amPm, Graphics.TEXT_JUSTIFY_LEFT);
		}
		
		drawSeconds(dc, false);
	}
	
	// Called to draw seconds both as part of full draw(), but also onPartialUpdate() of watch face in low power mode.
	// If isPartialUpdate flag is set to true, strictly limit the updated screen area: set clip rectangle before clearing old text
	// and drawing new. Clipping rectangle should not change between seconds.
	function drawSeconds(dc, isPartialUpdate) {
	
		var clockTime = System.getClockTime();
		var seconds = clockTime.sec.format("%02d");
		var secondsPositionY = (dc.getHeight() / 2) - 2;
		
		// helft van het scherm + helft dat de teksthoogte van de tijd - hoogte van het font van de seconden

		if (isPartialUpdate) {

			// @TODO x coordinaat vervangen door dynamische waarde
			dc.setClip(175, secondsPositionY, 25, 25);
			
			// Can't optimise setting colour once, at start of low power mode, at this goes wrong on real hardware: alternates
			// every second with inverse (e.g. blue text on black, then black text on blue).
			dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);

			// Clear old rect (assume nothing overlaps seconds text).
			dc.clear();

		} else {
		
			// Drawing will not be clipped, so ensure background is transparent in case font height overlaps with another drawable.
			dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
			
		}
		
		// @TODO x coordinaat vervangen door dynamische waarde
		dc.drawText(175, secondsPositionY, Graphics.FONT_SYSTEM_XTINY, seconds, Graphics.TEXT_JUSTIFY_LEFT);
		
	}
}