using Toybox.WatchUi;
using Toybox.System;
using Toybox.Application;

class Battery extends WatchUi.Drawable {
	
	function initialize(params) {
		Drawable.initialize(params);
	}
	
	function draw(dc) {
	
        var stats = System.getSystemStats();
		var battery = stats.battery;
		var batteryString = Lang.format("$1$%", [battery.toNumber()]);
		var batteryPositionX = dc.getWidth() / 2;

		// Draw the battery status        
		dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
		dc.drawText(batteryPositionX, 10, Graphics.FONT_XTINY, batteryString, Graphics.TEXT_JUSTIFY_CENTER);
		
	}

}