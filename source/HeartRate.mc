using Toybox.WatchUi;
using Toybox.System;
using Toybox.Application;
using Toybox.Activity;
using Toybox.ActivityMonitor;

class HeartRate extends WatchUi.Drawable {
	
	function initialize(params) {
		Drawable.initialize(params);
	}
	
	function draw(dc) {
	
		var activityInfo = Activity.getActivityInfo();
		var sample = activityInfo.currentHeartRate;
		var value = null;
		
		if (sample != null) {
		
			value = sample.format("%d");
			
		} else if (ActivityMonitor has :getHeartRateHistory) {
		
			sample = ActivityMonitor.getHeartRateHistory(1, /* newestFirst */ true).next();
			
			if ((sample != null) && (sample.heartRate != ActivityMonitor.INVALID_HR_SAMPLE)) {
				value = sample.heartRate.format("%d");
			}
		}

		// Draw the battery status        
		dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
		dc.drawText(75, 150, Graphics.FONT_XTINY, value, Graphics.TEXT_JUSTIFY_CENTER);
		
	}

}